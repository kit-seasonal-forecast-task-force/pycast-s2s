#Berechnung der CDFs der historischen SEAS5-Dateien

start=time.time()

mo=5
v=0

lati=103
loni=480
window=15   #shouldn't be bigger than 30 days, else the code of ERA5 has Problems

if int(721/lati) != 721/lati:
    print('Anzahl der Kacheln für Latitude geht nicht auf, bitte ändern.')
if int(1440/loni) != 1440/loni:
    print('Anzahl der Kacheln für Longitude geht nicht auf, bitte ändern.')

for latitude in range (1,2):  #int(721/lati)):
    for longitude in range (int(1440/loni)):
        #SEAS5-Daten laden
        seas=[]
        for yr in range (1981,2017):
            for ens in range (25):
                seas.append(np.asarray(getattr(xr.open_dataset('/bg/data/s2s/global/BCSD/Downscaled_SEAS5/ECMWF_SEAS5_025deg_'+format(ens,'02')+'_'+str(yr)+format(mo,'02')+'_'+var[v]+
                                                               '.nc'),var[v])[:,latitude*lati:(latitude+1)*lati,longitude*loni:(longitude+1)*loni]))
            print(yr)
        #Weil die historischen SEAS5-Daten noch nicht im korrekten Raster vorliegen, benutze ich als Ersatz vorerst die Jahre 2021 & 2022, die bereits gut sind
        #year=0
        #for yr in range (2021,2024):
        #    year=year+1
        #    for ens in range (51):
        #        seas.append(np.asarray(getattr(xr.open_dataset('/Volumes/pd/data/regclim_data/gridded_data/seasonal_predictions/seas5/daily/'+str(yr)+'/'+format(mo,'02')+
        #'/ECMWF_SEAS5_'+format(ens,'02')+'_'+
        #                  str(yr)+format(mo,'02')+'.nc'),var[v])[:,latitude*lati:(latitude+1)*lati,longitude*loni:(longitude+1)*loni]))
        seas=np.asarray(seas)
        end=time.time()
        print(end-start,'SEAS5 eingeladen')
        
        #Block zur Berechnung der normalen Tage
        q_fcsti=[]
        p_fcsti=[]
        dry_fcst=[]
        for days in range (215):
            start_day=days-window
            end_day=days+window+1
            if start_day<0:
                start_day=0
            if end_day > 215:
                end_day=215
            seas_hist=seas[:,start_day:end_day]
            nfcst = seas.shape[0]*(end_day-start_day) #36*25*(end_day-start_day)
            seas_hist=seas_hist.reshape(nfcst,lati,loni)
            
            p_min_fcst = 1 / (nfcst + 1)
            p_max_fcst = nfcst / (nfcst + 1)
            p_fcst = np.linspace(p_min_fcst, p_max_fcst, 200)  #nobs
            q_fcst = np.nanquantile(seas_hist, p_fcst, interpolation="midpoint",axis=0)
    
            # Remove the dublicate values
            #q_obs, ids_obs = np.unique(q_obs, return_index=True,axis=0)
            #p_obs = p_obs[ids_obs]
            
    
            q_fcsti.append(q_fcst)
            p_fcsti.append(p_fcst)
            if v == 0:
                #Berechnung der Dry Day Probability
                seas_hist[seas_hist>=0.1]=2
                seas_hist[seas_hist<0.1]=1
                seas_hist[seas_hist==2]=0
                dry_fcst.append(np.nansum(seas_hist,axis=0)/nfcst)
                
            #end=time.time()
            #print(days,end-start)
    
            
            #Speicherdatei schreiben
        
        #Beispielgrid einladen,variable doesn't matter
        gd=xr.open_dataset('/pd/home/weber-j/ERA5/ERA5_daily_tp_1981.nc').tp[:,latitude*lati:(latitude+1)*lati,longitude*loni:(longitude+1)*loni]
        lon = np.asarray(gd.lon[:loni])
        lat = np.asarray(gd.lat[:lati])
        day = np.arange(0,215)
        cdf = np.arange(1,201)
        if v == 0:
            ds = xr.Dataset(
                data_vars=dict(
                q_fcst=(["day","cdf", "lat", "lon"],q_fcsti),
                p_fcst=(["day","cdf"],p_fcsti),
                dry_fcst=(["day", "lat", "lon"],dry_fcst),
                ),
                coords=dict(
                    lon=("lon", lon),
                    lat=("lat", lat),
                    cdf=("cdf", cdf),
                    day=("day", day)
                )
            )
        else:
            ds = xr.Dataset(
                data_vars=dict(
                q_fcst=(["day","cdf", "lat", "lon"],q_fcsti),
                p_fcst=(["day","cdf"],p_fcsti)),
                coords=dict(
                    lon=("lon", lon),
                    lat=("lat", lat),
                    cdf=("cdf", cdf),
                    day=("day", day)
                )
            )
        ds.lat.attrs = dict(
            standard_name='latitude',
            long_name='latitude',
            units='degrees_north'
        )
        ds.lon.attrs = dict(
            standard_name='longitude',
            long_name='longitude',
            units='degrees_east'
        )
        ds.day.attrs = dict(
            long_name = 'days_since_first_of_issue_month',
        )
        ds.cdf.attrs = dict(
            standard_name='cdf',
            long_name='cummulative_distribution_function'
        )
        ds.q_fcst.attrs = dict(
            long_name = vari[v]+'_correction_CDF',
        )
        if v == 0:
            ds.dry_fcst.attrs = dict(
                long_name = 'dry_day_probability',
            )
        now=datetime.now()
        ds.attrs = {
            'title': 'CDF_SEAS5_'+var[v],
            'institution': 'Karlsruhe Institute of Technology - Institute of Meteorology and Climate Research',
            'institution_id': 'https://ror.org/040fv5d16', 
            'source': 'ECMWF SEAS5',
            'history': f"{now.strftime('%Y-%m-%dT%H:%M:%S')}: Dataset created",
            'references': 'TBA',
            'comment': "Calculated for a "+str(window)+"-day-window",
            'issue_month': format(mo,'02'),
            'Conventions': 'CF-1.8',
            'originator': 'Jan Niklas Weber',
            'originator_id': 'https://orcid.org/0000-0002-8596-6255',
            'contact': 'jan.weber@kit.edu',
            'crs': 'EPSG:4326',
            'licence': 'CC-BY 4.0'
        }
        
        if v == 0:
            encoding = {'lat': {'zlib': False, '_FillValue': None},
                        'lon': {'zlib': False, '_FillValue': None},
                        'q_fcst': {'scale_factor': 0.001,'_FillValue': -999.0, 'chunksizes': (1, 1, lati, loni), 'complevel': 1, 'zlib': True},
                        'p_fcst': {'scale_factor': 0.001,'_FillValue': -999.0, 'complevel': 1, 'zlib': True},
                        'dry_fcst': {'scale_factor': 0.001,'_FillValue': -999.0, 'chunksizes': (1, lati, loni), 'complevel': 1, 'zlib': True},
                        'cdf': {'dtype': 'int16'},
                        'day': {'dtype': 'int16'}}
        else:
            encoding = {'lat': {'zlib': False, '_FillValue': None},
                        'lon': {'zlib': False, '_FillValue': None},
                        'q_fcst': {'scale_factor': 0.001,'_FillValue': -999.0, 'chunksizes': (1, 1, lati, loni), 'complevel': 1, 'zlib': True},
                        'p_fcst': {'scale_factor': 0.001,'_FillValue': -999.0, 'complevel': 1, 'zlib': True},
                        'cdf': {'dtype': 'int16'},
                        'day': {'dtype': 'int16'}}
        
        ds.to_netcdf('/bg/data/s2s/global/CDF/SEAS5/'+var[v]+'/CDF'+format(mo,'02')+'_'+var[v]+'_sector'+str(int(latitude*(1440/loni)+longitude+1))+'.nc', encoding=encoding)
        
        #end=time.time()
        #print('Sektor '+str(int(latitude*(1440/loni)+longitude+1))+'/'+str(int((1440/loni)*(721/lati))),'\tZeit: '+str(np.round(end-start,1))+'s')
#print('finnisher')
