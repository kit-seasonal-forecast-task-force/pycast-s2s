#!/bin/bash

############# To run the script ###########################
#          nohup bash run_SEAS5.sh > output.log 2>&1 &
###########################################################
eval "$(conda shell.bash hook)"
conda activate pycast-s2s


year=$(date +%Y)
month=$(date +%m)
day=$(date +%d)

# Display date information
echo "Year: $year"
echo "Month: $month"

# Common options for Python scripts
select="-N 1 -n 30 -p rome"

# Change directory and check for errors
cd /bg/home/s_s2s/pycast-s2s/src 

# Variables and options for forecast processing
variables=("t2m" "tp")
domains=("tabn_chirps" "tabn")
stages=("forecasts")
functions=("truncate" "remap")

# Initial unpacking loop
for var in "${variables[@]}"; do
    name="-v ${var}"
    echo "Executing: $initial_command"
    initial_command="python3 process_global_forecasts.py -m unpack_forecasts -M ${month} -Y ${year} ${select} ${name}"
    
    if ! eval "$initial_command"; then
        echo "Error: Unpack job failed for variable ${var}."
        exit 1
    fi

    # Regional forecast processing loop
    for dom in "${domains[@]}"; do
        # Skip processing if dom is tabn_chirps and var is t2m
        if [[ "$dom" == "tabn_chirps" && "$var" == "t2m" ]]; then
            echo "Skipping variable t2m for domain tabn_chirps"
            continue
        fi

        for sta in "${stages[@]}"; do
            for fun in "${functions[@]}"; do
                process="${fun}_${sta}"
                command_proc="python3 process_regional_forecasts.py -d ${dom} -M ${month} -Y ${year} -m ${process} ${select} ${name}"
                echo "Executing: $command_proc"
                if ! eval "$command_proc"; then
                    echo "Error: Command $command_proc failed."
                    exit 1
                fi
            done
        done       
    done
    # Final BCSD command
        final_command="python3 run_bcsd.py -d ${dom} -Y ${year} -M ${month} -c false -s 4D ${select} ${name}"
        echo "Executing: $final_command"
        if ! eval "$final_command"; then
            echo "Error: BCSD command failed for domain ${dom}."
            exit 1
        fi
done

