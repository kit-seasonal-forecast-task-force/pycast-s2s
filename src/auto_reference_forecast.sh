#!/bin/bash

cd /bg/home/s_s2s/pycast-s2s/src || exit 1  

year=$(date +%Y)
month=10
day=$(date +%d)

echo "Year: $year"
echo "Month: $month"
echo "Day: $day"

first=1
select="-N 1 -n 30 -p rome"  # Adjust as necessary for your settings

# Initialize the variable to hold the previous job ID
previous_job=""

# Loop through each variable and submit jobs in sequence with dependencies
for var in tp t2m; do
    name="-v ${var}"
    unpack_command="python3 process_global_forecasts.py -m unpack_forecasts -M ${month} -Y ${year} ${select} ${name}"
    
    # Submit the "unpack" job and store the job ID
    if [ "$first" -eq 1 ]; then
        previous_job=$(sbatch --wrap="$unpack_command")
        previous_job=$(echo $previous_job | awk '{print $4}')  # Extract the job ID
        first=0
    fi
    
    # Submit the regional processing jobs with dependency on the previous job
    for dom in tabn_chirps; do
        for func in truncate remap concat rechunk; do
            for sta in reference forecasts; do
                if [ "$func" == "concat" ]; then
                    status="${func}_${sta}_daily"
                else
                    status="${func}_${sta}"
                fi

                # Construct the command for each step
                command="python3 process_regional_forecasts.py -d ${dom} -M ${month} -Y 1981,0,2016 -m ${status} ${select} ${name}"

                # Submit the job with a dependency on the previous job
                current_job=$(sbatch --dependency=afterok:${previous_job} --wrap="$command")
                current_job=$(echo $current_job | awk '{print $4}')  # Get job ID of current job

                # Set the current job ID as the new "previous" job
                previous_job=$current_job
            done
        done
    done
done

