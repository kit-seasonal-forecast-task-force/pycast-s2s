# import packages
import argparse
import json
import logging
import pandas as pd
import dask
import numpy as np
import xarray as xr
from tqdm import tqdm
from dask.distributed import Client
from datetime import datetime

import os
#import helper_modules
#from bc_module import bc_module

from modules import helper_modules
from modules.bc_module import bc_module, quantile_mapping, extremes_correction, precip_correction, bc_module_test
from modules import cluster_modules

def get_clas():
    # insert period, for which the bcsd-should be running! similar to process_regional_forecast
    parser = argparse.ArgumentParser(
        description="Python-based BCSD",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-d", "--domain", action="store", type=str, help="Domain", required=True
    )

    parser.add_argument(
        "-Y",
        "--Years",
        action="store",
        type=str,
        help="Years for which the processing should be executed",
        required=False,
    )

    parser.add_argument(
        "-M",
        "--Months",
        action="store",
        type=str,
        help="Months for which the processing should be executed",
        required=False,
    )

    parser.add_argument(
        "-v",
        "--variables",
        type=lambda s: s.split(","),  # Convert comma-separated string into a list
        help="Comma-separated list of variables",
        required=False,
    )
    
    parser.add_argument(
        "-c",
        "--crossval",
        action="store",
        type=str,
        help="If TRUE, do not use actual forecast for computing forecast climatology ",
        required=False,
    )

    parser.add_argument(
        "-s",
        "--forecast_structure",
        action="store",
        type=str,
        help="Structure of the line-chunked forecasts (can be 5D or 4D)",
        required=True,
    )

    parser.add_argument(
        "-N",
        "--nodes",
        action="store",
        type=int,
        help="Number of nodes for running the code",
        required=False,
    )

    parser.add_argument(
        "-n",
        "--ntasks",
        action="store",
        type=int,
        help="Number of tasks / CPUs",
        required=False,
    )

    parser.add_argument(
        "-p",
        "--partition",
        action="store",
        type=str,
        help="Partition to which we want to submit the job",
        required=False,
    )

    parser.add_argument(
        "-f",
        "--scheduler_file",
        action="store",
        type=str,
        help="""If a scheduler-file is provided, the function does not start its own cluster 
            but rather uses a running environment""",
        required=False,
    )

    parser.add_argument(
        "-C",
        "--CDF",
        action="store",
        type=bool,
        help="""Whether the pre-calculated CDFs should be used or not""",
        required=False,
    )
    
    parser.add_argument(
        "-rf",
        "--raw_forecast",
        type=str,
        help="Path to raw forecast file",
        required=False
    )

    return parser.parse_args()


def setup_logger(domain_name):
    logging.basicConfig(
        filename=f"logs/{domain_name}_run_bcsd.log",
        encoding="utf-8",
        level=logging.INFO,
        format="%(asctime)s:%(levelname)s:%(message)s",
    )


def process_sector(sector, fcst_path, raw_full_path):
    """Process one sector on a worker node"""
    # Load datasets directly on the worker
    fcst = xr.open_dataset(fcst_path)
    with open('/bg/data/s2s/'+args.domain+'/auto/checkpoint0_sector'+str(sector+1)+'.txt', 'w') as output:
        output.write('1')
    ds_pred = xr.open_dataset(raw_full_path).sel(
        latitude=slice(float(fcst.lat[0]), float(fcst.lat[-1])),
        longitude=slice(float(fcst.lon[0]), float(fcst.lon[-1]))
    ).rename({'latitude': 'lat', 'longitude': 'lon'})
    # If the interpolation creates values below 0, they are set to 0
    mo = int(ds_pred.time[0].dt.month)
    yr = int(ds_pred.time[0].dt.year)
    directory = f'/bg/data/s2s/{args.domain}/auto/'
    if f'SEAS5_BCSD_{yr}{format(mo, "02")}_{variable}_sector{sector + 1}.nc' not in lis:
        # SEAS5 has negative tp values, have to be corrected. If we change this in global_processing.py, this line can be deleted
        if variable == 'tp':
            ds_pred['tp'] = ds_pred['tp'].where(ds_pred['tp'] >= 0, 0)
        
        # Chunk time, leave others unchunked for full parallelization
        da_pred = ds_pred[variable].chunk({"time": 1, "lat": -1, "lon": -1}).persist()
    
        # Initialize result dataset
        da_temp = xr.full_like(da_pred, np.nan).persist()
        da_temp=da_temp.transpose('time','lat','lon','ens')
        q_obs=xr.open_dataset('/bg/data/s2s/global/CDF/ERA5/'+variable+'/CDF'+format(mo,'02')+'_ERA5_'+variable+'_q_obs_sector'+str(sector+1)+'.nc').q_obs
        p_obs=xr.open_dataset('/bg/data/s2s/global/CDF/ERA5/'+variable+'/CDF'+format(mo,'02')+'_ERA5_'+variable+'_p_obs_sector'+str(sector+1)+'.nc').p_obs
        p_obs=p_obs.expand_dims(lat=fcst.q_fcst["lat"], lon=fcst.q_fcst["lon"],axis=(-2, -1))
        if precip == True:
            p_dry_obs=xr.open_dataset('/bg/data/s2s/global/CDF/ERA5/'+variable+'/CDF'+format(mo,'02')+'_ERA5_'+variable+'_p_dry_obs_sector'+str(sector+1)+'.nc').p_dry_obs
        p_fcst = fcst.p_fcst.expand_dims(lat=fcst.q_fcst["lat"], lon=fcst.q_fcst["lon"], axis=(-2, -1))

        # Time loop
        liste=os.listdir(f'/bg/data/s2s/{args.domain}/auto/')
        for timestep in range(len(ds_pred.time)):
            if f'SEAS5_BCSD_{yr}{format(mo, "02")}_{variable}_sector{sector + 1}_{timestep}.csv' not in liste:
                da_temp[timestep, :, :] = xr.apply_ufunc(
                    bc_module_test,
                    da_pred.isel(time=timestep),
                    q_obs.isel(day=timestep),
                    fcst.q_fcst.isel(day=timestep),
                    p_obs.isel(day=timestep),
                    p_fcst.isel(day=timestep),
                    fcst.dry_fcst.isel(day=timestep),
                    p_dry_obs.isel(day=timestep),
                    kwargs={
                        "domain_config": domain_config["bc_params"],
                        "precip": variable_config[variable]["isprecip"],
                    },
                    input_core_dims=[["ens"], ["cdf"], ["cdf"], ["cdf"], ["cdf"], [], []],
                    output_core_dims=[["ens"]],
                    vectorize=True,
                    dask="parallelized",
                    output_dtypes=[np.float64],
                )
    # Overwrite the biggest data monster
    da_temp=0




if __name__ == "__main__":

    print(f"[run_bcsd] ----------------------------------")
    print(f"[run_bcsd]       Pycast S2S Main program     ")
    print(f"[run_bcsd] ----------------------------------")
    print(f"[run_bcsd]             Version 0.1           ")
    print(f"[run_bcsd] ----------------------------------")

    # Read the command line arguments
    args = get_clas()

    # Create a new logger file (or append to an existing file)
    setup_logger(args.domain)

    # Read the domain configuration from the respective JSON
    # Create a new logger file (or append to an existing file)
    setup_logger(args.domain)

    # Read the domain configuration from the respective JSON
    with open("conf/domain_config.json", "r") as j:
        domain_config = json.loads(j.read())

    # Read the global configuration from the respective JSON --> Add this as further input parameter
    with open("conf/attribute_config.json", "r") as j:
        attribute_config = json.loads(j.read())

    # Read the variable configuration from the respective JSON
    with open("conf/variable_config.json", "r") as j:
        variable_config = json.loads(j.read())

    try:
        domain_config = domain_config[args.domain]
    except:
        logging.error(f"Init: no configuration for domain {args.domain}")
        sys.exit()

    if args.variables is not None:
        variable_config = {
            key: value
            for key, value in variable_config.items()
            if key in args.variables
        }
    else:
        variable_config = {
            key: value
            for key, value in variable_config.items()
            if key in domain_config["variables"]
        }

    reg_dir_dict, glob_dir_dict = helper_modules.set_and_make_dirs(domain_config)

    process_years = helper_modules.decode_processing_years(args.Years)

    if args.Months is not None:
        process_months = helper_modules.decode_processing_months(args.Months)

    syr_calib = domain_config["syr_calib"]
    eyr_calib = domain_config["eyr_calib"]

    # Get some ressourcers
    if args.partition is not None:
    client, cluster = cluster_modules.getCluster(
        args.partition,
        args.nodes,
        args.ntasks,
        walltime='2-00:00:00',
        cores=32,   # Ensure enough cores for parallel tasks
        memory='360GB'  # Adjust memory to avoid OOM issues
    )

    client.get_versions(check=True)
    client.amm.start()

    print(f"[run_bcsd] Dask dashboard available at {client.dashboard_link}")

    if args.scheduler_file is not None:
        client = Client(scheduler_file=args.scheduler_file)

        client.get_versions(check=True)
        client.amm.start()

        print(f"[run_bcsd] Dask dashboard available at {client.dashboard_link}")

    if args.CDF == True:
        for year in process_years:
    
            for month in process_months:
    
                for variable in variable_config:
    
                    print(f"[run_bcsd] Starting BC-routine for year {year}, month {month} and variable {variable} with pre-calculated CDFs")
                    
                    ds_pred = xr.open_dataset(raw_full)
                    ds_pred = xr.open_mfdataset(
                        raw_full,
                        chunks={
                            "time": len(ds_pred.time),
                            "ens": len(ds_pred.ens),
                            "lat": 'auto',
                            "lon": 'auto',
                        },
                        parallel=True,
                        engine="netcdf4",
                    )
                    da_pred = ds_pred[variable].persist()

                    # I need that for the processing the things
                    mo = int(ds_pred.time[0].dt.month)
                    yr = int(ds_pred.time[0].dt.year)
                    if variable == 'tp':
                        precip=True
                    else:
                        precip=False

                    if domain_config["prefix"] == "global":

                        # In my experience, some workers get killed. So I check for unfinished business and run it again

                        for i in range (2):
                            # Collect the missing sector names
                            missing=[]
                            lis=os.listdir('/bg/data/s2s/global/auto/')
                            for sector in range(21):
                                if 'SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'.nc' not in lis:
                                    if 'SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'_213.csv' not in lis:
                                        missing.append(sector)
                            
                            # No scattering of datasets — only paths
                            future_fcst_paths = ['/bg/data/s2s/global/CDF/SEAS5/' + variable + '/CDF' + format(month, '02') + '_' + variable + '_sector' + str(sector + 1) + '.nc' for sector in missing]
                            raw_full_path = args.raw_forecast
    
                            # Starting all necessary sectors in parallel
                            futures = [
                                client.submit(process_sector, sector, fcst_path, raw_full_path)
                                for sector, fcst_path in zip(missing, future_fcst_paths)
                            ]

                        # Converting the csv-files to netCDF
                        encoding = {
                                    'lat': {'dtype': 'float32', 'zlib': False, '_FillValue': None},
                                    'lon': {'dtype': 'float32', 'zlib': False, '_FillValue': None},
                                    variable: {'dtype': 'int32', 'scale_factor': 0.001, '_FillValue': -999, 'complevel': 1, 'zlib': True},
                                    'ens': {'dtype': 'int16'},
                                    'time': {'dtype': 'float64', 'units': 'days since 1900-01-01','calendar': 'standard', 'zlib': True,'_FillValue': None}
                                    }
                        lis=os.listdir('/bg/data/s2s/global/auto/')
                        for sector in range(21):
                            if 'SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'.nc' not in lis:
                                if 'SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'_213.csv' in lis:
                                    df0 = pd.read_csv('/bg/data/s2s/global/auto/SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'_0.csv')
                                    time_value = pd.to_datetime(df0['time'].iloc[0])
                                    df0 = df0.drop(columns=['time'])
                                    ds0 = df0.set_index(['lat', 'lon', 'ens']).to_xarray()
                                    ds0 = ds0.expand_dims(time=[time_value])
                                    
                                    for i in range (1,215):
                                        df = pd.read_csv('/bg/data/s2s/global/auto/SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'_'+str(i)+'.csv')
                                        time_value = pd.to_datetime(df['time'].iloc[0])
                                        df = df.drop(columns=['time'])
                                        ds = df.set_index(['lat', 'lon', 'ens']).to_xarray()
                                        ds = ds.expand_dims(time=[time_value])
                                        ds0=xr.concat([ds0,ds],dim='time')
                                    
                                    # Save as NetCDF
                                    ds0=ds0.transpose('time','ens','lat','lon')
                                    ds0.to_netcdf('/bg/data/s2s/global/auto/SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sector+1)+'.nc', mode='w', format='NETCDF4',encoding=encoding)

                    # Die Dateien von Sektoren auf ens-member umstrukturieren
                    # Erst triggern, wenn alle Sektoren berechnet sind
                    # Muss mal wieder angeschaut werden, ob es nicht besser geht
                    
                    
                    lati=103
                    loni=480
                    
                    if year < 2017:
                        Ensnr=25
                    else:
                        Ensnr=51
                    
                    for ens in range (Ensnr):
                        sect=1
                        laa=[]
                        for lat in range (7):
                            la=[]
                            for lon in range (3):
                                la.append(np.asarray(getattr(xr.open_dataset('/bg/data/s2s/global/auto/SEAS5_BCSD_'+str(year)+format(month,'02')+'_'+variable+'_sector'+str(sect)+'.nc'),variable)[:,ens]))
                                sect=sect+1
                            laa.append(xr.concat(la,dim='lon').sortby('lat', ascending=False))
                        data=np.asarray(xr.concat(laa,dim='lat'))
                    
                        cdf_era=xr.open_dataset('/pd/data/regclim_data/gridded_data/reanalyses/era5/monthly/ERA5_monthly_tp_1981.nc').tp[0]
                        lon = np.asarray(cdf_era.longitude)
                        lat = np.asarray(cdf_era.latitude)
                        time = pd.date_range(str(year)+'-'+format(month,'02')+'-01', periods=215)
                    
                        data_dict={
                            variable: data
                        }
                        data_vars={}
                        data_vars[variable]=(["time","lat","lon"],data)
                        ds = xr.Dataset(
                            data_vars=data_vars,
                            coords=dict(
                                lon=("lon", lon),
                                lat=("lat", lat),
                                time=("time", time)
                            )
                        )
                        ds.lat.attrs = dict(
                            standard_name='latitude',
                            long_name='latitude',
                            units='degrees_north'
                        )
                        ds.lon.attrs = dict(
                            standard_name='longitude',
                            long_name='longitude',
                            units='degrees_east'
                        )
                        ds.time.attrs = dict(
                            standard_name = 'time',
                            long_name = 'time'
                        )
                        if variable == 'tp':
                            ds.tp.attrs = dict(
                                standard_name='precipitation_flux',
                                long_name='total_daily_precipitation',
                                units='mm day-1',
                            )
                        elif variable == 't2m':
                            ds.t2m.attrs = dict(
                                standard_name='air_temperature',
                                long_name='air_temperature_at_2m_heigth',
                                units='K',
                            )
                        elif variable == 't2max':
                            ds.t2max.attrs = dict(
                                standard_name='maximum_air_temperature',
                                long_name='maximum_air_temperature_at_2m_heigth',
                                units='K',
                            )
                        elif variable == 't2min':
                            ds.t2min.attrs = dict(
                                standard_name='minimum_air_temperature',
                                long_name='minimum_air_temperature_at_2m_heigth',
                                units='K',
                            )
                        elif variable == 'ssrd':
                            ds.ssrd.attrs = dict(
                                standard_name='surface_solar_radiation',
                                long_name='shortwave_radiation_at_2m_height',
                                units='W m-2 s-1',
                            )
                        elif variable == 'strd':
                            ds.strd.attrs = dict(
                                standard_name='surface_thermal_radiation',
                                long_name='longwave_radiation_at_2m_height',
                                units='W m-2 s-1',
                            )
                    
                        now=datetime.now()
                        ds.attrs = {
                            'title': 'SEAS5_BCSD_'+str(year)+format(month,'02'),
                            'institution': 'Karlsruhe Institute of Technology - Institute of Meteorology and Climate Research',
                            'institution_id': 'https://ror.org/040fv5d16', 
                            'source': 'ECMWF SEAS5, ECMWF ERA5',
                            'history': f"{now.strftime('%Y-%m-%dT%H:%M:%S')}: Dataset created",
                            'references': 'TBA',
                            'comment': '',
                            'Conventions': 'CF-1.8',
                            'originator': 'Jan Niklas Weber',
                            'originator_id': 'https://orcid.org/0000-0002-8596-6255',
                            'contact': 'jan.weber@kit.edu',
                            'crs': 'EPSG:4326',
                            'licence': 'CC-BY 4.0'
                        }
                        encoding = {'lat': {'zlib': False, '_FillValue': None},
                                    'lon': {'zlib': False, '_FillValue': None},
                                    variable: {'dtype': 'int32','scale_factor': 0.001,'_FillValue': -999.0, 'chunksizes': (1,len(cdf_era.latitude),len(cdf_era.longitude)), 'complevel': 1, 'zlib': True},
                                    'time': {'dtype': 'int16'}}
                    
                        #ds.to_netcdf('/bg/data/s2s/global/BCSD/'+str(yr)+format(mo,'02')+'/SEAS5_BCSD_'+str(yr)+format(mo,'02')+'_'+var[v]+'_'+format(ens,'02')+'.nc',encoding=encoding)
                        #Malte hätte lieber als naming convention:
                        ds.to_netcdf('/bg/data/s2s/global/BCSD/'+str(year)+format(month,'02')+'/seas5_'+str(year)+format(month,'02')+'_'+format(ens,'02')+'_'+variable+'_newmethod.nc', encoding=encoding)
                        
                    


                    
                    else:   # Not revised yet
                        # Loading the ERA5 CDFs and cutting them

                        if len(la)>1 and len(li) == 1:
                            name=[]
                            for i in range (len(la)):
                                name.append(xr.open_dataset('/bg/data/s2s/global/CDF/ERA5/'+var[v]+'/CDF_ERA5_'+var[v]+'_sector'+str(la[i]*3+li[0]+1)+
                                                                        '.nc').sel(lat=slice(latlonbox[3],latlonbox[2]),lon=slice(latlonbox[0],latlonbox[1])))
                            obs=xr.concat(name,dim='lat')
                        
                        
                        
                        if len(li)>1 and len(la) == 1:
                            name=[]
                            for i in range (len(li)):
                                name.append(xr.open_dataset('/bg/data/s2s/global/CDF/ERA5/'+var[v]+'/CDF_ERA5_'+var[v]+'_sector'+str(la[0]*3+li[i]+1)+
                                                                        '.nc').sel(lat=slice(latlonbox[3],latlonbox[2]),lon=slice(latlonbox[0],latlonbox[1])))
                            obs=xr.concat(name,dim='lon')
                        
                        
                        if len(li)>1 and len(la)>1:
                            sec_co=[]
                            for j in range (len(li)):
                                name=[]
                                for i in range (len(la)):
                                    name.append(xr.open_dataset('/bg/data/s2s/global/CDF/ERA5/'+var[v]+'/CDF_ERA5_'+var[v]+'_sector'+str(la[i]*3+li[j]+1)+
                                                                            '.nc').sel(lat=slice(latlonbox[3],latlonbox[2]),lon=slice(latlonbox[0],latlonbox[1])))
                                sec_co.append(xr.concat(name,dim='lat'))
                            obs=xr.concat(sec_co,dim='lon')
                        
                        # Loading the SEAS5 CDFs and cutting them
                        
                        if len(la)>1 and len(li) == 1:
                            name=[]
                            for i in range (len(la)):
                                name.append(xr.open_dataset('/bg/data/s2s/global/CDF/SEAS5/'+var[v]+'/CDF'+format(mo,'02')+'_'+var[v]+'_sector'+str(la[i]*3+li[0]+1)+
                                                                        '.nc').sel(lat=slice(latlonbox[3],latlonbox[2]),lon=slice(latlonbox[0],latlonbox[1])))
                            fcst=xr.concat(name,dim='lat')
                        
                        
                        
                        if len(li)>1 and len(la) == 1:
                            name=[]
                            for i in range (len(li)):
                                name.append(xr.open_dataset('/bg/data/s2s/global/CDF/SEAS5/'+var[v]+'/CDF'+format(mo,'02')+'_'+var[v]+'_sector'+str(la[0]*3+li[i]+1)+
                                                                        '.nc').sel(lat=slice(latlonbox[3],latlonbox[2]),lon=slice(latlonbox[0],latlonbox[1])))
                            fcst=xr.concat(name,dim='lon')
                        
                        
                        if len(li)>1 and len(la)>1:
                            sec_co=[]
                            for j in range (len(li)):
                                name=[]
                                for i in range (len(la)):
                                    name.append(xr.open_dataset('/bg/data/s2s/global/CDF/SEAS5/'+var[v]+'/CDF'+format(mo,'02')+'_'+var[v]+'_sector'+str(la[i]*3+li[j]+1)+
                                                                            '.nc').sel(lat=slice(latlonbox[3],latlonbox[2]),lon=slice(latlonbox[0],latlonbox[1])))
                                sec_co.append(xr.concat(name,dim='lat'))
                            fcst=xr.concat(sec_co,dim='lon')
                        
                    # Modifying the observations to fit the 215 days

                    mo=int(fg.time[0].dt.month)
                    
                    laa1=[]
                    laa2=[]
                    for lt in range (7):
                        mona=mo+lt-1
                        if mona>11:
                            mona=mona-12
                        laa1.append(np.asarray(obs.q_obs[int(np.sum(monate[:mona-1])):int(np.sum(monate[:mona]))]))
                        laa2.append(np.asarray(obs.p_obs[0,0,int(np.sum(monate[:mona-1])):int(np.sum(monate[:mona]))]))
                    laa1=np.concatenate(laa1,axis=0)[15:200]   #Because start and end needs special treatment
                    laa2=np.concatenate(laa2,axis=0)[15:200]
                    q_obs=np.concatenate([np.asarray(obs.q_obs_start[mo-1]),laa1,np.asarray(obs.q_obs_end[mona])],axis=0)
                    p_obs=np.concatenate([np.asarray(obs.p_obs_start[0,0,mo-1]),laa2,np.asarray(obs.p_obs_end[0,0,mona])],axis=0)
                    q_fcst=np.asarray(fcst.q_fcst)
                    p_fcst=np.asarray(fcst.p_fcst[0,0])
                    
                    if precip== True:
                        laa=[]
                        for lt in range (7):
                            mona=mo+lt-1
                            if mona>11:
                                mona=mona-12
                            laa.append(np.asarray(obs.dry_obs[int(np.sum(monate[:mona-1])):int(np.sum(monate[:mona]))]))
                        laa=np.concatenate(laa,axis=0)[15:200]
                        p_dry_obs=np.concatenate([np.asarray(obs.dry_obs_start[mo-1]),laa,np.asarray(obs.dry_obs_end[mona])],axis=0)
                        p_dry_fcst=np.asarray(fcst.dry_fcst)
                    else:
                        p_dry_fcst=[]
                        p_dry_obs=[]



                        
                    for timestep in tqdm(range(0, len(ds_pred.time))):

                        da_pred_sub = da_pred.isel(time=timestep)
    
                        da_temp[timestep, :, :] = xr.apply_ufunc(
                            bc_module_test,
                            da_pred_sub,
                            q_obs[timestep],
                            q_fcst[timestep],
                            p_obs[timestep],
                            p_fcst[timestep],
                            p_dry_fcst[timestep],
                            p_dry_obs[timestep],
                            kwargs={
                                "bc_params": domain_config["bc_params"],
                                "precip": variable_config[variable]["isprecip"],
                            },
                            input_core_dims=[["ens"], ["time"], ["ens_time"]],
                            output_core_dims=[["ens"]],
                            vectorize=True,
                            dask="parallelized",
                            output_dtypes=[np.float64],
                        )
    
                    # Change the datatype from "object" to "float64" --> Can we somehow get around this???
                    da_temp = da_temp.astype("float64")
    
                    # Select only the actual variable from the output dataset
                    # ds_out_sel = ds[[variable]]
    
                    # Fill this variable with some data...
                    ds[variable].values = da_temp.transpose(
                        "time", "ens", "lat", "lon"
                    ).values
    
                    # ...and save everything to disk..
                    # ds_out_sel.to_netcdf(bcsd_dict[variable], mode='a', format='NETCDF4_CLASSIC', engine='netcdf4', encoding = {variable: encoding[variable]})
                    ds.to_netcdf(
                       pp_full,
                       mode="w",
                       engine="netcdf4",
                       encoding={
    			variable: encoding[variable], 
    			'ens': encoding['ens'],
    			'time': encoding['time'],
    			'lat': encoding['lat'],
    			'lon': encoding['lon']
    		   },
                    )




    
    else:
        
        # Insert IF-Statement in order to run the bcsd for the historical files
        for year in process_years:
    
            for month in process_months:
    
                for variable in variable_config:
    
                    print(f"[run_bcsd] Starting BC-routine for year {year}, month {month} and variable {variable}")
    
                    (
                        raw_full,
                        pp_full,
                        refrcst_full,
                        ref_full,
                    ) = helper_modules.set_input_files(
                        domain_config, reg_dir_dict, month, year, variable
                    )
    
                    coords = helper_modules.get_coords_from_frcst(raw_full)
    
                    global_attributes = helper_modules.update_global_attributes(
                        attribute_config, domain_config["bc_params"], coords, args.domain
                    )
    
                    encoding = helper_modules.set_encoding(variable_config, coords)
    
                    ds = helper_modules.create_4d_netcdf(
                        pp_full,
                        global_attributes,
                        domain_config,
                        variable_config,
                        coords,
                        variable,
                    )
    
                    print(f"[run_bcsd] Using {refrcst_full} as reference for the calibration period")
                    print(f"[run_bcsd] Using {ref_full} as forecasts for the calibration period")
                    print(f"[run_bcsd] Using {raw_full} as actual forecasts")
    
                    ds_obs = xr.open_zarr(ref_full, consolidated=False)
    
                    ds_obs = xr.open_zarr(
                        ref_full,
                        chunks={"time": len(ds_obs.time), "lat": 10, "lon": 10},
                        consolidated=False
                        # parallel=True,
                        # engine="netcdf4",
                    )
                    da_obs = ds_obs[variable].persist()
    
                    if args.forecast_structure == "5D":
                        ds_mdl = helper_modules.preprocess_mdl_hist(
                            refrcst_full, month, variable
                        )  # chunks={'time': 215, 'year': 36, 'ens': 25, 'lat': 1, 'lon': 1})
                        da_mdl = ds_mdl.persist()
                    elif args.forecast_structure == "4D":
                        ds_mdl = xr.open_zarr(refrcst_full, consolidated=False)
                        ds_mdl = xr.open_zarr(
                            refrcst_full,
                            chunks={
                                "time": len(ds_mdl.time),
                                "ens": len(ds_mdl.ens),
                                "lat": 'auto',
                                "lon": 'auto',
                            },
                            consolidated=False
                            # parallel=True,
                            # engine="netcdf4",
                        )
                        da_mdl = ds_mdl[variable].persist()
                        
                    
                    # Pred (current year for one month and 215 days)
                    ds_pred = xr.open_dataset(raw_full)
                    ds_pred = xr.open_mfdataset(
                        raw_full,
                        chunks={
                            "time": len(ds_pred.time),
                            "ens": len(ds_pred.ens),
                            "lat": 'auto',
                            "lon": 'auto',
                        },
                        parallel=True,
                        engine="netcdf4",
                    )
                    da_pred = ds_pred[variable].persist()
    
                    
                    
                    if args.crossval == True:
                        da_mdl = da_mdl.sel(time=~da_pred.time)
                        da_obs = da_obs.sel(time=~da_pred.time)
    
                    da_temp = xr.DataArray(
                        None,
                        dims=["time", "lat", "lon", "ens"],
                        coords={
                            "time": (
                                "time",
                                coords["time"],
                                {"standard_name": "time", "long_name": "time"},
                            ),
                            "ens": (
                                "ens",
                                coords["ens"],
                                {
                                    "standard_name": "realization",
                                    "long_name": "ensemble_member",
                                },
                            ),
                            "lat": (
                                "lat",
                                coords["lat"],
                                {
                                    "standard_name": "latitude",
                                    "long_name": "latitude",
                                    "units": "degrees_north",
                                },
                            ),
                            "lon": (
                                "lon",
                                coords["lon"],
                                {
                                    "standard_name": "longitude",
                                    "long_name": "longitude",
                                    "units": "degrees_east",
                                },
                            ),
                        },
                    ).persist()
    
                    print(f"Starting BC-routine for year {year}, month {month} and variable {variable}")
    
                    for timestep in tqdm(range(0, len(ds_pred.time))):
                    #for timestep in range(0,4):
    
                        #print(f"Correcting timestep {timestep}...")
                        # MDL: Get all model-timesteps as days of year
                        dayofyear_mdl = da_mdl["time.dayofyear"]
                        # MDL: Select the day at the position timestep
                        day = dayofyear_mdl[timestep]
    
                        for calib_year in range(syr_calib, eyr_calib + 1):
                            
                            
    
                            # OBS: Select all days of the current year
                            ds_obs_year = ds_obs.sel(time=ds_obs.time.dt.year == calib_year)
                            # OBS: Transform dates into days of year
                            dayofyear_obs = ds_obs_year["time.dayofyear"]
    
                            # normal years
                            # OBS: Check if the current year is a leap year or not
                            if len(ds_obs_year.time.values) == 365:
                                
                                day_range = (
                                    np.arange(
                                        day - domain_config['bc_params']['window'] - 1,
                                        day + domain_config['bc_params']['window'],
                                    )
                                    + 365
                                ) % 365 + 1
    
                                # leap years
                            else:
                                day_range = (
                                                    np.arange(
                                                        day - domain_config['bc_params']['window'] - 1,
                                                        day + domain_config['bc_params']['window'],
                                                    )
                                                    + 366
                                            ) % 366 + 1
    
         
                            intersection_day_obs_year = np.in1d(dayofyear_obs, day_range)
    
                            if calib_year == syr_calib:
                                intersection_day_obs = intersection_day_obs_year
                            else:
                                intersection_day_obs = np.append(
                                    intersection_day_obs, intersection_day_obs_year
                                )
                        # Make subset of obs data
                        da_obs_sub = da_obs.loc[dict(time=intersection_day_obs)]
    
                        # get actual day of timestep in MDL data
                        for i in range(0, 7740, 215):
                            da_mdl_215 = ds_mdl["time.dayofyear"][i:i + 215]
                            years_mdl = ds_mdl["time.year"][i:i + 215]
    
                            day_normal = ds_mdl["time.dayofyear"][i:i + 215][timestep]
                            date_min = day_normal.time - np.timedelta64(15, "D")
                            date_max = day_normal.time + np.timedelta64(15, "D")
    
                            date_range = pd.date_range(date_min.values, date_max.values)
    
                            day_range = date_range.dayofyear
                            year_range = date_range.year
    
                            # Correct for leap years
                            # day_range_2 = np.where((day_range>=60) & ((year_range==1984) | (year_range==1988) | (year_range==1992) | (year_range==1996) | (year_range==2000)| (year_range==2004)| (year_range==2008)| (year_range==2012)| (year_range==2016)),day_range+1, day_range+0)
    
                            # day_range = (np.arange(day_normal - 15 - 1,day_normal + 15,)+ 365) % 365 + 1
                            if i == 0:
                                intersection_day_mdl = da_mdl_215.isin(day_range)
                            else:
                                intersection_day_mdl = np.append(intersection_day_mdl, da_mdl_215.isin(day_range))
    
                        da_mdl_sub = da_mdl.loc[dict(time=intersection_day_mdl)]
                        da_mdl_sub = da_mdl_sub.stack(
                            ens_time=("ens", "time"), create_index=True
                        )
                        da_mdl_sub = da_mdl_sub.drop_vars("time")
    
                        #Rechunk in time
                        # da_mdl_sub = da_mdl_sub.chunk({"ens_time": -1})
                        # da_obs_sub = da_obs_sub.chunk({"time": -1})
    
                        # Select current timestep in prediction data
                        da_pred_sub = da_pred.isel(time=timestep)
    
    
    
                        da_temp[timestep, :, :] = xr.apply_ufunc(
                            bc_module,
                            da_pred_sub,
                            da_obs_sub,
                            da_mdl_sub,
                            kwargs={
                                "bc_params": domain_config["bc_params"],
                                "precip": variable_config[variable]["isprecip"],
                            },
                            input_core_dims=[["ens"], ["time"], ["ens_time"]],
                            output_core_dims=[["ens"]],
                            vectorize=True,
                            dask="parallelized",
                            output_dtypes=[np.float64],
                        )
    
                    # Change the datatype from "object" to "float64" --> Can we somehow get around this???
                    da_temp = da_temp.astype("float64")
    
                    # Select only the actual variable from the output dataset
                    # ds_out_sel = ds[[variable]]
    
                    # Fill this variable with some data...
                    ds[variable].values = da_temp.transpose(
                        "time", "ens", "lat", "lon"
                    ).values
    
                    # ...and save everything to disk..
                    # ds_out_sel.to_netcdf(bcsd_dict[variable], mode='a', format='NETCDF4_CLASSIC', engine='netcdf4', encoding = {variable: encoding[variable]})
                    ds.to_netcdf(
                       pp_full,
                       mode="w",
                       engine="netcdf4",
                       encoding={
    			variable: encoding[variable], 
    			'ens': encoding['ens'],
    			'time': encoding['time'],
    			'lat': encoding['lat'],
    			'lon': encoding['lon']
    		   },
                    )

                # ds_out_sel.close()

    # client.close()

    # if cluster is not None:
    #     cluster.close()

    # coords = helper_modules.get_coords_from_frcst(list(raw_dict.values())[0])

    # Update Filenames:
    # fnme_dict = dir_fnme.set_filenames(domain_config, syr_calib, eyr_calib, year, month_str, domain_config['bcsd_forecasts']['merged_variables'])

    # Get directories for the corresponding variable
    # if args.domain == 'germany':
    # (
    #    raw_dict,
    #    bcsd_dict,
    #    ref_hist_dict,
    #    mdl_hist_dict,
    # ) = helper_modules.set_filenames(
    #    domain_config,
    #    variable_config,
    #    dir_dict,
    #    syr_calib,
    #    eyr_calib,
    #    year,
    #    month_str,
    #    False,
    # )
    # else:
    #     raw_dict, bcsd_dict, ref_hist_dict, mdl_hist_dict = helper_modules.set_filenames(args.year, args.month, domain_config, variable_config, True)

    # IMPLEMENT A CHECK IF ALL INPUT FILES ARE AVAILABL!!!!!!

    # Read the dimensions for the output file (current prediction)

    # attribute_config = helper_modules.update_global_attributes(
    #    attribute_config, domain_config["bc_params"], coords, args.domain
    # )

    # encoding = helper_modules.set_encoding(variable_config, coords)

    # Loop over each variable
    # for variable in variable_config:
    # Create an empty NetCDF in which we write the BCSD output
    #    ds = helper_modules.create_4d_netcdf(
    #        bcsd_dict,
    #        attribute_config,
    #        domain_config,
    #        variable_config,
    #        coords,
    #        variable,
    #    )

    ###### Old IO-Module #####
    # load data as dask objects
    # print(f"Opening {ref_hist_dict[variable]}")
    # ds_obs = xr.open_dataset(ref_hist_dict[variable])
    # ds_obs = xr.open_mfdataset(
    #    ref_hist_dict[variable],
    #    chunks={"time": len(ds_obs.time), "lat": 50, "lon": 50},
    #    parallel=True,
    #    engine="netcdf4",
    # )
    # da_obs = ds_obs[variable].persist()

    # Mdl (historical, 1981 - 2016 for one month and 215 days)  215, 36, 25, 1, 1 ;
    # Preprocess historical mdl-data, create a new time coord, which contain year and day at once and not separate
    # print(f"Opening {mdl_hist_dict[variable]}")
    # if args.forecast_structure == "5D":
    #    ds_mdl = helper_modules.preprocess_mdl_hist(
    #        mdl_hist_dict[variable], month_str, variable
    #    )  # chunks={'time': 215, 'year': 36, 'ens': 25, 'lat': 1, 'lon': 1})
    #    da_mdl = ds_mdl.persist()
    # elif args.forecast_structure == "4D":
    #    ds_mdl = xr.open_mfdataset(mdl_hist_dict[variable])
    #    ds_mdl = xr.open_mfdataset(
    #        mdl_hist_dict[variable],
    #        chunks={
    #            "time": len(ds_mdl.time),
    #            "ens": len(ds_mdl.ens),
    #            "lat": 5,
    #            "lon": 5,
    #        },
    #        parallel=True,
    #        engine="netcdf4",
    #    )
    #    da_mdl = ds_mdl[variable].persist()

    # IMPLEMENT ELSE-Statement for logging

    # Pred (current year for one month and 215 days)
    # ds_pred = xr.open_dataset(raw_dict[variable])
    # ds_pred = xr.open_mfdataset(
    #    raw_dict[variable],
    #    chunks={
    #        "time": len(ds_pred.time),
    #        "ens": len(ds_pred.ens),
    #        "lat": 50,
    #        "lon": 50,
    #    },
    #    parallel=True,
    #    engine="netcdf4",
    # )
    # da_pred = ds_pred[variable].persist()

    # IF ABFAGE OB AKTUELLE VORHERSAGE AUS HISTORIE ENTFERNT WERDEN SOLL
    # if args.crossval == True:
    #    da_mdl = da_mdl.sel(time=~da_pred.time)
    #    da_obs = da_obs.sel(time=~da_pred.time)#

    # Change data type of latidude and longitude, otherwise apply_u_func does not work
    # da_pred = da_pred.assign_coords(lon=ds_pred.lon.values.astype(np.float32), lat=ds_pred.lat.values.astype(np.float32))

    # Calculate day of the year from time variable
    # dayofyear_obs = ds_obs['time.dayofyear']
    # dayofyear_mdl = ds_mdl['time.dayofyear']

    # da_temp = xr.DataArray(
    #    None,
    #    dims=["time", "lat", "lon", "ens"],
    #    coords={
    #        "time": (
    #            "time",
    #            coords["time"],
    #            {"standard_name": "time", "long_name": "time"},
    #        ),
    #        "ens": (
    #            "ens",
    #            coords["ens"],
    #            {
    #                "standard_name": "realization",
    #                "long_name": "ensemble_member",
    #            },
    #        ),
    #        "lat": (
    #            "lat",
    #            coords["lat"],
    #            {
    #                "standard_name": "latitude",
    #                "long_name": "latitude",
    #                "units": "degrees_north",
    #            },
    #        ),
    #        "lon": (
    #            "lon",
    #            coords["lon"],
    #            {
    #                "standard_name": "longitude",
    #                "long_name": "longitude",
    #                "units": "degrees_east",
    #             #            },
    #             #        ),
    #             #    },
    #             #).persist()

    #             for timestep in range(0, len(ds_pred.time)):
    #                 # for timestep in range(82, 83):

    #                 print(f"Correcting timestep {timestep}...")
    #                 dayofyear_mdl = ds_mdl["time.dayofyear"]
    #                 day = dayofyear_mdl[timestep]

    #                 # Deal with normal and leap years
    #                 for year in range(syr_calib, eyr_calib + 1):

    #                     ds_obs_year = ds_obs.sel(time=ds_obs.time.dt.year == year)
    #                     ds_mdl_year = ds_mdl.sel(time=ds_mdl.time.dt.year == year)
    #                     dayofyear_obs = ds_obs_year["time.dayofyear"]
    #                     dayofyear_mdl = ds_mdl_year["time.dayofyear"]

    #                     # normal years
    #                     if len(ds_obs_year.time.values) == 365:

    #                         # day_range = (np.arange(day - domain_config['bc_params']['window'], day + domain_config['bc_params']['window'] + 1) + 365) % 365 + 1
    #                         day_range = (
    #                             np.arange(
    #                                 day - domain_config["bc_params"]["window"] - 1,
    #                                 day + domain_config["bc_params"]["window"],
    #                             )
    #                             + 365
    #                         ) % 365 + 1

    #                         # leap years
    #                     else:
    #                         day_range = (
    #                             np.arange(
    #                                 day - domain_config["bc_params"]["window"] - 1,
    #                                 day + domain_config["bc_params"]["window"],
    #                             )
    #                             + 366
    #                         ) % 366 + 1

    #                     intersection_day_obs_year = np.in1d(dayofyear_obs, day_range)
    #                     intersection_day_mdl_year = np.in1d(dayofyear_mdl, day_range)

    #                     if year == syr_calib:
    #                         intersection_day_obs = intersection_day_obs_year
    #                         intersection_day_mdl = intersection_day_mdl_year
    #                     else:
    #                         intersection_day_obs = np.append(
    #                             intersection_day_obs, intersection_day_obs_year
    #                         )
    #                         intersection_day_mdl = np.append(
    #                             intersection_day_mdl, intersection_day_mdl_year
    #                         )

    #                     # print(f'Correcting timestep {timestep}...')

    #                     # day = dayofyear_mdl[timestep]

    #                     # day_range = (np.arange(day - domain_config['bc_params']['window'], day + domain_config['bc_params']['window'] + 1) + 365) % 365 # +1
    #                     # intersection_day_obs = np.in1d(dayofyear_obs, day_range)
    #                     # intersection_day_mdl = np.in1d(dayofyear_mdl, day_range)

    #                 da_obs_sub = da_obs.loc[dict(time=intersection_day_obs)]
    #                     ens_time=("ens", "time"), create_index=True
    #                 )
    #                 da_mdl_sub = da_mdl_sub.drop("time")

    #                 # If the actual year of prediction (e.g. 1981) is within the calibration period (1981 to 2016): cut this year out in both, the historical obs and mdl data
    #                 # da_obs_sub = da_obs_sub.sel(time=~da_obs_sub.time.dt.year.isin(args.year))
    #                 # da_mdl_sub = da_mdl_sub.sel(time=~da_mdl_sub.time.dt.year.isin(args.year))

    #                 da_pred_sub = da_pred.isel(time=timestep)

    #                 da_temp[timestep, :, :] = xr.apply_ufunc(
    #                     bc_module,
    #                     da_pred_sub,
    #                     da_obs_sub,
    #                     da_mdl_sub,
    #                     kwargs={
    #                         "bc_params": domain_config["bc_params"],
    #                         "precip": variable_config[variable]["isprecip"],
    #                     },
    #                     input_core_dims=[["ens"], ["time"], ["ens_time"]],
    #                     output_core_dims=[["ens"]],
    #                     vectorize=True,
    #                     dask="parallelized",
    #                     output_dtypes=[np.float64],
    #                 )

    #             # Change the datatype from "object" to "float64" --> Can we somehow get around this???
    #             da_temp = da_temp.astype("float64")

    #             # Select only the actual variable from the output dataset
    #             ds_out_sel = ds[[variable]]

    #             # Fill this variable with some data...
    #             ds_out_sel[variable].values = da_temp.transpose(
    #                 "time", "ens", "lat", "lon"
    #             ).values

    #             # ...and save everything to disk..
    #             # ds_out_sel.to_netcdf(bcsd_dict[variable], mode='a', format='NETCDF4_CLASSIC', engine='netcdf4', encoding = {variable: encoding[variable]})
    #             ds_out_sel.to_netcdf(
    #                 bcsd_dict[variable],
    #                 mode="a",
    #                 engine="netcdf4",
    #                 encoding={variable: encoding[variable]},
    #             )
    #             # ds_out_sel.close()

    # # client.close()

    # # if cluster is not None:
    # #     cluster.close()
