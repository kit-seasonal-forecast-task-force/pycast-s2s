#!/bin/bash

cd /bg/home/s_s2s/pycast-s2s/src 

year=$(date +%Y)
month=10  
day=$(date +%d)

# Display the results
echo "Year: $year"
echo "Month: $month"
echo "Day: $day"

first=1


for var in tp t2m; do
  for dom in tabn_chirps; do
    for func in unpack_forecasts truncate remap concat rechunk; do
      for sta in reference forecasts; do 
        select="-N 1 -n 30 -p milan"
        name="-v ${var}"

        if [ "$func" == "concat" ]; then
          status="${func}_${sta}_daily"
        else
          status="${func}_${sta}"
        fi
        
        if [ "$func" == "unpack_forecasts" ] ; then 
           command="python3 process_regional_forecasts.py unpack_forecasts -M ${month} -Y ${year} ${select} ${name}"
        else
        
           command="python3 process_regional_forecasts.py -d ${dom} -M ${month} -Y 1981,0,2016 -m ${status} ${select} ${name}"
	fi
  
        if [ ${first} -eq 1 ]; then
          previous=$(sbatch --wrap="${command}")
          previous=$(echo $previous | awk '{print $4}')  
          first=0
        else
          current=$(sbatch --dependency=afterok:${previous} --wrap="${command}")
          current=$(echo $current | awk '{print $4}')  
          previous=${current}  
        fi
      done
    done
  done
done

