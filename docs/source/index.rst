.. PyCast S2S documentation master file, created by
   sphinx-quickstart on Sat Nov 18 23:16:39 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyCast S2S's documentation!
======================================
PyCast S2S is a Python-Framework that contains tools for post-processing forecast data. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Regional processing modules 
----------------------------
.. automodule:: modules.regional_processing_modules
   :members:
   :undoc-members:
   :show-inheritance:

Run_bcsd - The main file of PyCast S2S
---------------------------------------

The helper_modules
------------------
.. automodule:: modules.helper_modules
   :members:
   :undoc-members:
   :show-inheritance:

The cluster_modules
-------------------
.. automodule:: modules.cluster_modules
   :members:
   :undoc-members:
   :show-inheritance:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
